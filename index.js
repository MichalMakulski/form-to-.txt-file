var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require("fs");

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.render('index', { title: 'Form to txt file', message: 'Add note!'});
});

app.post('/handle-form', function (req, res) {
    
    var noteTitle = req.body.noteTitle;
    var noteContents = req.body.noteContents;

    fs.writeFile('notes/' + noteTitle + '.txt', noteContents, function(err) {
        if (err) {
            console.log(err);
            return;
        }
        res.render('message', {title:'Form submitted', message: 'Note added :-)'});
    });
    
});

app.get('/my-notes', function(req, res) {
    if (req.query.hasOwnProperty('note')) {
        fs.readFile('notes/' + req.query.note, 'utf-8', function(err, contents){
            res.json({contents: contents});
        });
        return;
    }
    fs.readdir('notes/', function(err, files) {
        var notes = files.map(function(file){
            return {title: file};
        });
        
        res.json(notes);
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});