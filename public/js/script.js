var AJAX = (function() {
	var xmlhttp = new XMLHttpRequest(); 

	function get (src, query) {  
		query = query || '';
		return new Promise(function(resolve, reject) {
			xmlhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {
					resolve(this.responseText);
				} 
			};
			xmlhttp.open("GET", src + query, true);
			xmlhttp.send();
		});
	}

	return {
		get: get
	}
})();

function buildList (data) {
    var list = document.querySelector('.notes-list');
    var listItems = JSON.parse(data).map(function(val){
        return '<li><a class="note-link" href="#">' + val.title + '</a></li>';
    }).join('');
    list.innerHTML = listItems;
}

function showNoteContents (noteData) {
    var note = JSON.parse(noteData);
    var noteElt = document.querySelector('.current-note');
    noteElt.innerHTML = '<p>'+note.contents+'</p>';
}

function log(data) {
    console.log(data);
}

function getNotes(ev) {
    var target = ev.target;
    var getNotesBtn = document.getElementById('my-notes');
    var isNoteLink = target.classList.contains('note-link');
    

    if (target === getNotesBtn) {
        AJAX.get('/my-notes').then(buildList);
    }
    if (isNoteLink) {
        ev.preventDefault();
        var query = '?note=' + target.textContent;
        AJAX.get('/my-notes', query).then(showNoteContents);
    }
 
}

document.addEventListener('click', getNotes);